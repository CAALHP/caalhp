﻿namespace caalhp.Core.Library.Managers
{
    public interface ILifeCycleManager
    {
        void StartPlugins();
        void StopPlugins();
    }
}