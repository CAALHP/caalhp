﻿using System.Collections.Generic;

namespace caalhp.Core.Library.Hosts
{
    public interface IServiceHost : IHost
    {
        void ActivateDrivers();
        IList<string> GetListOfEventTypes();
    }
}