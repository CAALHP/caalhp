﻿namespace caalhp.Core.Library.Visitor
{
    public interface IHostVisitable
    {
        void Accept(IHostVisitor visitor);
    }
}