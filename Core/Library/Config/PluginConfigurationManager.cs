﻿using System.Xml;
using System.Xml.Serialization;

namespace caalhp.Core.Library.Config
{
    public static class PluginConfigurationManager
    {
        public static CareStorePluginConfig GetConfig(string configPath)
        {
            var factory = new XmlSerializerFactory();
            var serializer = factory.CreateSerializer(typeof(CareStorePluginConfig));
            var reader = new XmlTextReader(configPath);
            if (serializer == null || !serializer.CanDeserialize(reader)) return null;
            var plugin = serializer.Deserialize(reader) as CareStorePluginConfig;
            return plugin;
        }
    }
}
