﻿using System;
using caalhp.Core.Events;

namespace caalhp.Core.Library.Observer
{
    public class UserLoggedInEventArgs : EventArgs
    {
        public UserLoggedInEvent UserEvent { get; private set; }

        public UserLoggedInEventArgs(UserLoggedInEvent userEvent)
        {
            UserEvent = userEvent;
        }
    }
}