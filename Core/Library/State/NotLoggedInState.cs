using System;
using System.Collections.Generic;
using caalhp.Core.Events.Types;
using caalhp.Core.Library.Config;
using caalhp.Core.Library.Visitor;

namespace caalhp.Core.Library.State
{
    public class NotLoggedInState : IUserState
    {
        /// <summary>
        /// CAALHP is a context reference
        /// </summary>
        private readonly ICAALHP _caalhp;

        public NotLoggedInState(ICAALHP caalhp)
        {
            _caalhp = caalhp;
            Console.WriteLine("No user logged in");
        }

        public void UserLoggedIn(User user)
        {
            Console.WriteLine("Logging in user: " + user.FirstName + " " + user.LastName);
            _caalhp.UserState = new UserLoggedInState(_caalhp);
        }

        public void UserLoggedOut()
        {
            
        }

        public IList<PluginConfig> GetListOfInstalledApps()
        {
            var visitor = new GetListOfInstalledAppsNotLoggedInVisitor();
            _caalhp.Accept(visitor);
            return visitor.Result;
        }
    }
}