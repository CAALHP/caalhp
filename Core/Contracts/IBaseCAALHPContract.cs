﻿using System.Collections.Generic;

namespace caalhp.Core.Contracts
{
    public interface IBaseCAALHPContract
    {
        void Notify(KeyValuePair<string, string> notification);
        string GetName();
        bool IsAlive();
        void ShutDown();
    }
}