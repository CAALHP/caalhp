﻿using caalhp.Core.Library.Hosts;
using Ice;

namespace caalhp.Core.SoaIce.Host.HostAdapters
{
    public interface IDeviceDriverHostIce : IDeviceDriverHost
    {
        void RegisterDeviceDriver(string name, string category, int processId, Current current__);
    }
}