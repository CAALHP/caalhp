﻿using System.Collections.Generic;
using System.Diagnostics;
using caalhp.Core.Events;
using caalhp.Core.Library.Config;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using CAALHP.SOAICE.Contracts;

namespace caalhp.Core.SoaIce.Host.Processes
{
    public class ServiceProcess : BaseProcess
    {
        public IServiceContractPrx Service { get; set; }
        public ServiceProcess(Process process, PluginConfig config) : base(process, config) { }
        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (Service != null) Service.begin_Notify(theEvent.Key, theEvent.Value);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            var newEvent = new NewEventTypeAddedEvent
            {
                EventType = newFqns,
                CallerName = GetType().FullName,
                //CallerProcessId = Id
            };
            var value = EventHelper.CreateEvent(SerializationType.Json, newEvent);
            Update(value);
        }
    }
}