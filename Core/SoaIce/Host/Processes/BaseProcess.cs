﻿using System.Collections.Generic;
using System.Diagnostics;
using caalhp.Core.Library.Config;
using caalhp.Core.Library.Observer;

namespace caalhp.Core.SoaIce.Host.Processes
{
    public abstract class BaseProcess : IEventObserver
    {
        public Process ProcessReference { get; private set; }
        public int ProcessId { get; private set; }
        //public PerformanceCounter PerfCounter { get; private set; }
        public int RestartCount { get; set; }
        public PluginConfig Config { get; private set; }

        protected BaseProcess(Process process, PluginConfig config)
        {
            ProcessReference = process;
            ProcessId = ProcessReference.Id;
            Config = config;
            Init();
        }

        private async void Init()
        {
            //await InitPerformanceCounter();
        }

        /*private async Task InitPerformanceCounter()
        {
            await Task.Run(() => { PerfCounter = PerformanceCounterHelper.GetPerformanceCounterByProcessId(ProcessId); });
        }*/

        public abstract void Update(KeyValuePair<string, string> theEvent);
        public abstract void NewEventTypeUpdate(string newFqns);
    }
}