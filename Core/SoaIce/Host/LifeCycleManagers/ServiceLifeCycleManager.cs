﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using caalhp.Core.Library.Config;
using caalhp.Core.SoaIce.Host.Processes;
using System.Threading;

namespace caalhp.Core.SoaIce.Host.LifeCycleManagers
{
    public class ServiceLifeCycleManager : LifeCycleManager
    {
        public Dictionary<int, ServiceProcess> ServiceDictionary { get; private set; }
        //private readonly string _pluginRoot;

        public ServiceLifeCycleManager(string pluginRoot)
            : base(pluginRoot)
        {
            ServiceDictionary = new Dictionary<int, ServiceProcess>();
            //_pluginRoot = pluginRoot;
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var serviceProcess in ServiceDictionary)
            {
                var process = serviceProcess;
                Task.Run(() =>
                {
                    if (process.Value == null || process.Value.Service == null) return;
                    try
                    {
                        if (!process.Value.Service.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception)
                    {
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        public override async Task ActivatePlugin(PluginConfig plugin)
        {
            //await Task.Run(() =>
            //{
                if (PluginIsActive(plugin)) return;
                //start the first file
                //var processStartInfo = //new ProcessStartInfo(Path.Combine(plugin.Directory, plugin.Config.RunFiles.First()));
                var plugInToLaunch = Path.Combine(plugin.Directory, plugin.Config.RunFiles.First());
                //processStartInfo.
                //var plugInToLaunch = Path.Combine(plugin.Directory, plugin.Config.RunFiles.First());
                ProcessStartInfo info = new ProcessStartInfo();
                info.WindowStyle = ProcessWindowStyle.Minimized;
                info.UseShellExecute = true;
                //info.WorkingDirectory = plugin.Directory;
                info.FileName = plugInToLaunch;

                KillPredesseors(plugInToLaunch);
                var process = Process.Start(info);
                var pluginProcess = new ServiceProcess(process, plugin);
                if (process != null)
                {
                    ServiceDictionary.Add(process.Id, pluginProcess);
                    Console.WriteLine("activated service: " + plugin.Config.PluginName);
                }
            //});
        }



        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var serviceProcess in ServiceDictionary.Values.Where(serviceProcess => serviceProcess.Service != null))
            {
                //We expect communication to break after this call, so we will not wait for a response.
                try
                {

                    serviceProcess.Service.begin_ShutDown();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error during StopPlugins: " + e.Message);
                }
            }
            
            //Allow for a grace period of 300 ms for applications to gracefully shutdown before killing them via the OS
            Thread.Sleep(300);

            foreach (var serviceProcess in ServiceDictionary.Values.Where(serviceProcess => serviceProcess.Service != null))
            {
                try
                {
                    //We expect communication to break after this call, so we will not wait for a response.
                    Process.GetProcessById(serviceProcess.ProcessId).Kill();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Process " + serviceProcess.ProcessId + " was already closed - as planned: " + e.Message);
                }
            }

        }

        protected override int GetRestartCounter(int processId)
        {
            return ServiceDictionary.ContainsKey(processId) ? ServiceDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (ServiceDictionary.ContainsKey(processId)) ServiceDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            var config = ServiceDictionary[key].Config;
            if (config == null) return;
            var filename = Path.Combine(config.Directory, config.Config.RunFiles.First());
            if (!File.Exists(filename)) return;
            var process = Process.Start(filename);
            if (process == null) return;
            var pluginProcess = new ServiceProcess(process, config) { RestartCount = ServiceDictionary[key].RestartCount };
            ServiceDictionary.Add(process.Id, pluginProcess);
            ServiceDictionary.Remove(key);

            Console.WriteLine("reactivated app: " + config.Config.PluginName);

            try
            {
                if (Process.GetProcesses().Any(p => p.Id == key))
                {
                    var oldProcess = Process.GetProcessById(key);
                    if (oldProcess != null) oldProcess.Kill();
                }
            }
            catch
            {
                Console.WriteLine("Attempted to kill the old version of the reactivated process: " + key);
            }
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!ServiceDictionary.ContainsKey(processId)) return;
                var view = ServiceDictionary[processId].Service;
                if (view == null) return;
                ServiceDictionary[processId].Service = null;
                view.ShutDown();
            }
            catch (Exception)
            {
                Console.WriteLine("plugin was already closed");
            }
            finally
            {
                if (Process.GetProcesses().Any(pr => pr.Id == processId))
                    Process.GetProcessById(processId).Kill();
            }
        }

        private bool PluginIsActive(PluginConfig config)
        {
            return ServiceDictionary.Values.Any(serviceProcess => serviceProcess.Config.Directory.Equals(config.Directory));
        }
    }
}