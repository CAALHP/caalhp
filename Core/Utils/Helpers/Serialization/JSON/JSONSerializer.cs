﻿using System;
using Newtonsoft.Json;

namespace caalhp.Core.Utils.Helpers.Serialization.JSON
{
    public static class JsonSerializer
    {
        public static string SerializeEvent<T>(T eventToSerialize)
        {
            var json = JsonConvert.SerializeObject(eventToSerialize);
            return json;
        }

        public static T DeserializeEvent<T>(string json)
        {
            var theEvent = JsonConvert.DeserializeObject<T>(json);
            return theEvent;
        }

        public static object DeserializeEvent(string json, Type T)
        {
            var theEvent = JsonConvert.DeserializeObject(json, T);
            return Convert.ChangeType(theEvent, T);
        }

        

    }
}
