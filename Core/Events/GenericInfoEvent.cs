﻿namespace caalhp.Core.Events
{
    public class GenericInfoEvent : Event
    {
        public string Info { get; set; }
    }
}