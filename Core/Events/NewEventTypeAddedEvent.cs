namespace caalhp.Core.Events
{
    public class NewEventTypeAddedEvent : Event
    {
        public string EventType { get; set; }
    }
}