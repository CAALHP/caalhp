﻿namespace caalhp.Core.Events
{
    public class InstallAppCompletedEvent : Event
    {
        public string AppName { get; set; }
        public string FileName { get; set; }
    }
}