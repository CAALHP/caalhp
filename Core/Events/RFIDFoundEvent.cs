﻿namespace caalhp.Core.Events
{
    public class RFIDFoundEvent : Event
    {
        public string Tag { get; set; }
    }
}
