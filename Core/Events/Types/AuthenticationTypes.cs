﻿using System;

namespace caalhp.Core.Events.Types
{
    [Obsolete]
    public enum RequestTypes
    {
        AuthByFinger,
        AuthByPincode,
        Enroll,
        CheckforScanner,
        DeleteUser,
        ClearDb
    }
}
