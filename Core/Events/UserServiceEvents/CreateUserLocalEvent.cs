﻿using System;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events.UserServiceEvents
{
    [Obsolete]
    public class CreateUserLocalEvent: Event
    {
        public UserProfile UserProfile { get; set; }
        public string PhotoRoot { get; set; }
        public bool NewPhoto { get; set; }
    }
}
