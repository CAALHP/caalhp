﻿using System;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events.UserServiceEvents
{
    [Obsolete]
    public class BiometricsResponsEvent : Event
    {
        public BiometricDeviceType Type { get; set; }
    }
}
