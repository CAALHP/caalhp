﻿using System;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events.UserServiceEvents
{
    [Obsolete]
    public class IsUserLoggedInResponsEvent : Event
    {
        public UserProfile User { get; set; }
        public string PhotoRoot { get; set; }
    }
}
