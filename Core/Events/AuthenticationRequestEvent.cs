﻿using System;
using caalhp.Core.Events.Types;

namespace caalhp.Core.Events
{
    [Obsolete]
    public class AuthenticationRequestEvent : Event
    {
        public string Username { get; set; }
        public string Pincode { get; set; }
        public RequestTypes ReqType { get; set; }
    }
}
