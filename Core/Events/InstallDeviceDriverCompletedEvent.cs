﻿namespace caalhp.Core.Events
{
    public class InstallDeviceDriverCompletedEvent : Event
    {
        public string DeviceDriverFileName { get; set; }
    }
}
