﻿namespace caalhp.Core.Events
{
    public class DownloadDeviceDriverCompletedEvent : Event
    {
        public string DeviceDriverFileName { get; set; }
    }
}
