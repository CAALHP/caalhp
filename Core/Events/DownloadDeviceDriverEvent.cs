﻿namespace caalhp.Core.Events
{
    public class DownloadDeviceDriverEvent : Event
    {
        public string DeviceDriverFileName { get; set; }
    }
}
