﻿using System;
using System.Collections.Generic;
using System.Linq;
using caalhp.Core.Contracts;
using caalhp.Core.Library.Hosts;
using CAALHP.SOAICE.Contracts;
using IPluginInfo = caalhp.Core.Contracts.IPluginInfo;

namespace caalhp.IcePluginAdapters
{
    public class AppHostToCAALHPContractAdapter : IAppHostCAALHPContract
    {
        public IHostCAALHPContract Host { get; set; }
        private readonly IAppHostContractPrx _appHostContract;

        public AppHostToCAALHPContractAdapter(IAppHostContractPrx host)
        {
            if (host == null) throw new ArgumentNullException("host");
            Console.WriteLine("Constructing ServiceHostToCAALHPContractAdapter");
            _appHostContract = host;
            try
            {
                Console.WriteLine("_appHostContract.GetHost");
                Host = new HostToCAALHPContractAdapter(_appHostContract);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void ShowApp(string appName)
        {
            _appHostContract.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _appHostContract.CloseApp(appName);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _appHostContract.GetListOfInstalledApps().Select(pluginInfo => new PluginInfo() { LocationDir = pluginInfo.LocationDir, Name = pluginInfo.Name }).Cast<IPluginInfo>().ToList();
        }
    }
}
