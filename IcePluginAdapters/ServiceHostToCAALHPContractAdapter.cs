﻿using System;
using System.Collections.Generic;
using System.Linq;
using caalhp.Core.Contracts;
using caalhp.Core.Library.Hosts;
using CAALHP.SOAICE.Contracts;
using IPluginInfo = caalhp.Core.Contracts.IPluginInfo;

namespace caalhp.IcePluginAdapters
{
    public class ServiceHostToCAALHPContractAdapter : IServiceHostCAALHPContract
    {
        public IHostCAALHPContract Host { get; set; }
        private readonly IServiceHostContractPrx _serviceHostContract;

        public ServiceHostToCAALHPContractAdapter(IServiceHostContractPrx host)
        {
            if (host == null) throw new ArgumentNullException("host");
            Console.WriteLine("Constructing ServiceHostToCAALHPContractAdapter");
            _serviceHostContract = host;
            try
            {
                Console.WriteLine("_serviceHostContract.GetHost");
                Host = new HostToCAALHPContractAdapter(_serviceHostContract);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _serviceHostContract.GetListOfInstalledApps().Select(pluginInfo => new PluginInfo() {LocationDir = pluginInfo.LocationDir, Name = pluginInfo.Name}).Cast<IPluginInfo>().ToList();
        }

        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _serviceHostContract.GetListOfInstalledDeviceDrivers().Select(pluginInfo => new PluginInfo() { LocationDir = pluginInfo.LocationDir, Name = pluginInfo.Name }).Cast<IPluginInfo>().ToList();
        }

        public void CloseApp(string fileName)
        {
            _serviceHostContract.CloseApp(fileName);
        }

        public void ActivateDeviceDrivers()
        {
            _serviceHostContract.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return _serviceHostContract.GetListOfEventTypes();
        }
    }
}